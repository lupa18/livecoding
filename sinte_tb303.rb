# Welcome to Sonic Pi v2.10
live_loop :pepe do
  use_synth :tb303
  play 38
  sleep 0.25
  play 50
  sleep 0.25
  play 62
  use_synth :tri
  sleep 0.25
  play 38
  sleep 0.25
  play [65,70,80,4].choose
  sleep 0.25
  play 62
  sleep 0.25
end